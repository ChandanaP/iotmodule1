**INDUSTRIAL REVOLUTION**

![](Industrial4.jpg)


---------------------------------------------------------------------------------
**Industry 3.0** 
---------------------------------------------------------------------------------
_Workflow: Sensors --> PLC --> SCADA & ERP_

_Communication Protocols:The Sensors installed in the factory send data to PLCs by means of Field Bus, which are basically certain Protocols such as Modbus, CANopen, EtherCAT, etc._

![Architecture of Industry 3.0](a3.png)

-----------------------------------------------------------------------------------
**Industry 4.0**
-----------------------------------------------------------------------------------

_Industry 4.0 is Industry 3.0 connected to Internet, which is called IoT._

**IIoT** _includes Showing Dashboards , Remote Web SCADA , Remote control configuration of devices , Predictive maintenance , Real-time event processing , Analytics with predictive models , Automated device provisioning (Auto discovery) , Real-time alerts & alarms.  etc..
Communication Protocols:Some Protocols used to send data to cloud for data analysis are : MQTT.org, AMQP, OPC UA, CoAP RFC 7252, HTTP, WebSocket, RESTful API etc._

![Architecture of Industry 4.0](a4.png)


**What is the difference between Industry 3.0 and Industry 4.0?**

_In Industry 3.0, we automate processes using logic processors and information technology. These processes often operate largely without human interference, but there is still a human aspect behind it. Where Industry 4.0 comes in is with the availability and use of vast quantities of data on the production floor._
![](com.jpeg)

**Interaction between hierarchies Levels in Industry 3.0 and Industry 4.0**

![](h.png)



---------------------------------------------------------------


**Problems with Industry 4.0 upgrades**

* _Cost_
   * _Factory owners don't want to switch into Industry 4.0, because it is Expensive._
* _Downtime_
   * _Changing Hardware would result in downtime and nobody want to face such loss._
* _Reliablity_
   * _Changing Hardware would result in downtime and nobody want to face such loss._

**Solution:**

* _What we can do is getting data from Industry 3.0 devices/meters/sensors without changes to the original device.And then send the data to the Cloudusing Industry 4.0 devices._
   * _Collecting data from industry 3.0 devices_
   * _Sending data into industry 4.0 devices_ 
   
**How do we get data from Industry 3.0 device ?**  

_Convert Industry 3.0 protocols to Industry 4.0 protocols_

**Challenges inConversion:**

* _Expensive Hardware_ 
* _Lack of documentaion_
* _Properitary PLC protocols_

![](l.png)

**Roadmap for making yourown Industrial IoT product**

* _Step1 : Identify mostpopular Industry 3.0devices._
* _Step2 : Study Protocolsthat these devicesCommunicate._
* _Step3 : Get data fromthe Industry 3.0 devices._
* _Step4 : Send the data tocloud for Industry 4.0._


**IoT tools** : `Store your data in Time series databases`

_Examples:_
   * _Prometheus_
   * _InfluxDB_ 

**IoT Dashboards** : `View all your data into beautiful dashboards`

_Examples:_
   * _Grafana_
   * _Thingsboard_

**IoT Platforms** : `Analyse your data on these platforms`

_Examples:_
   * _AWS IoT_
   * _Google IoT_
   * _Azure IoT_






